﻿using UnityEngine;
using System.Collections;

public class enemyTransitionController : MonoBehaviour {

    public Animator anim;
    public GameObject jugador;

    private Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        // obtenemos el animator controller del esqueleto
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        Transform tr = jugador.GetComponent<Transform>();
        // si la posicion del esqueleto es distinta de la del jugador
        if(GetComponent<Transform>().position != tr.position)
        {
            // se mueve hacia el jugador
            float xComp = tr.position.x - GetComponent<Transform>().position.x;
            float yComp = tr.position.y - GetComponent<Transform>().position.y;

            Vector2 temporal = new Vector2(xComp, yComp);

            rb.velocity = temporal * 1;
        }


        if (Input.GetKeyDown(KeyCode.W))
        {
            // accion... 
            anim.SetInteger("state", 2);
        }
        if (Input.GetKeyUp(KeyCode.W)) {
            // acciones...
            anim.SetInteger("state", 0);
        }
	}


}
