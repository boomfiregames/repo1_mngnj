﻿using UnityEngine;
using System.Collections;

public class mover : MonoBehaviour , IDamager<int>{

    public int velocidad = 20;
    /**
      Bearing values:
        0 : right
        1 : left
        2 : up
        3 : down
    */
    public int direccion = 0;
    private Rigidbody2D rb2d;

    void Start()
    {
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        // establece direccion
        switch (direccion) {
            case 0:     // right
                rb2d.velocity = transform.right * velocidad;
                transform.Rotate(0, 0, 90);
                break;
            case 1:     // left
                rb2d.velocity = transform.right * (-velocidad);
                transform.Rotate(0, 0, 90);
                break;
            case 2:     // up
                rb2d.velocity = transform.up * velocidad;
                
                break;
            case 3:     // down
                rb2d.velocity = transform.up * (-velocidad);
                
                break;
            default:
                break;
        }
    }

    void OnColliderEnter2D(Collision2D other)
    {
        if(other.gameObject.tag != "Player")
        {
            Destroy(this.gameObject);
        }
        else
        {
            if(other.gameObject.tag == "Villano")
            {
                //
            }
        }
    }

    // se activa cuando un rigidbody entra en contacto con el objeto
    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("proyectil choca con elemento");
        if(other.gameObject.GetComponent<enemigo>() is IDamageable<float>)
        {
            DealDamage(10, other.gameObject);
            Debug.Log("... y le daña");
        }
        else
        {
            Debug.Log("...y resulta no ser dañable: "+other.name);
        }
    }


    // metodo de la interfaz idamager<t>
    public void DealDamage(int damageDealt, GameObject other)
    {
        other.GetComponent<enemigo>().recibir(damageDealt); 
        Debug.Log("*nombre = "+other.name);
    }

}
