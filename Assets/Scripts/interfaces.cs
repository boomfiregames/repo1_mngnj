﻿using UnityEngine;
using System.Collections;


// interfaz para moverse (enemigos)
public interface IMoveable
{
    void mover();
}

// interfaz para hacer pupa
public interface IDamager<T>
{
    void DealDamage(T damageDealt, GameObject other);
}


// interfaz para recibir pupa
public interface IDamageable<T>
{
    T vidaMax { get; set; }
    T vidaCurr { get; set; }
    void Damage(T damageTaken);
}

// interfaz para morir
public interface IKillable
{
    void Kill();
}
