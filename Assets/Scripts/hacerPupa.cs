﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class hacerPupa : MonoBehaviour {

    public float vidaMax = 100f;
    private float vidaCurr;


    public Color verde = Color.green;
    public Color rojo = Color.red;
    public Slider slider;
    public Image img;

    public GameObject proyectil;
    public GameObject shotSpawnDer;

	// Use this for initialization
	void Start () {
        vidaCurr = vidaMax;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.L))
        {
            Instantiate(proyectil, shotSpawnDer.transform.position, Quaternion.identity);
        }
	}
    // recibir daño
    public void recibir(float pupa)
    {
        vidaCurr = vidaCurr - pupa;
            
        img.color = Color.Lerp(rojo, verde, vidaCurr / vidaMax);
        slider.value = vidaCurr;
    }
}
