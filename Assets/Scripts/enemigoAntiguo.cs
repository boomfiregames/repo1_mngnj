﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class enemigoAntiguo : MonoBehaviour, IKillable, IDamageable<float>, IDamager<float> {

    /*
    * enum definiendo el tipo de enemigo segun ataque (por el momento)
     los posibles valores son: 
     0  -  melee
     1  -  distancia
     ...
    public int tipo_enemigo = 0;
    */
    /* DECLARACION DE VARIABLES */

    public Slider barraVida;                // barra de vida             no olvidar incluir "using UnityEngine.UI" en la cabecera
    public float poder = 40f;               // poder de ataque
    public float init_vidaMax = 100f;       // vida maxima      REDUNDANTE?
    public Color verde = Color.green;
    public Color rojo = Color.red;
    public Image img;                       // imagen de relleno de la barra de vida
    public GameObject Jugador;
    public float velocidad;
    public int max_velocidad = 30;
    public int alcance;                     // limite del ataque
    //public int vision;                      // limite de vision
    private float _vidaMax;                 // vida maxima
    private float _vidaCurr;                // vida actual
    private bool puede_moverse;
    private Rigidbody2D rb;                 // componente de este objeto

    public float tiempoAtaque;          // hara de cronometro
    public float cadencia = 3;              // tiempo que tarda en disparar

    // VARIABLES DE INTERFAZ
    // Este sistema se hace para proteger el acceso a la variable. Suele ser siempre igual.

    public float vidaMax                    // REDUNDANTE?
    {
        get { return _vidaMax; }            // metodo para leer la variable
        set { _vidaMax = value; }           // metodo para modificar la variable
    }

    public float vidaCurr
    {
        get { return _vidaCurr; }
        set { _vidaCurr = value; }
    }

    /*************************************************************
        FUNCIONES DE UNITY
    *************************************************************/


    /**
    * Start
    * Esta funcion se ejecuta una sola vez al crear el objeto.
    * Pondremos todas las operaciones de inicializacion aqui.
    */
    void Start () {/*----------------------------------
        foreach(Transform child in transform)
        {
            if (child.name == "healthSlider")
            {
                Debug.Log("encontrado");
                barraVida = child.gameObject;
            }
            else
            {
                Debug.Log("no encontrado : "+child.name);
            }
        }----------------------------------------------------*/
        
        // establece la vida inicial
        vidaMax = init_vidaMax;
        vidaCurr = init_vidaMax;
        puede_moverse = false;
        rb = GetComponent<Rigidbody2D>();       // obtiene el componente Rigidbody2D de este objeto
        velocidad = 10;
        tiempoAtaque = Time.time;
    }

    /**
     * Update
     * Esta funcion se ejecuta ciclicamente durante el juego.
     * Desde aqui se controlaran las acciones del enemigo, que se ejecutaran de forma automatica a lo largo del juego.
     */
    void Update () {
        // Debug.Log("vel = " + rb.velocity.magnitude);
        if (Jugador == null) { Debug.Log("!!!!!!!!!!!!!!!!!!!"); }
        else {
            if (rb.velocity.magnitude < max_velocidad)
            {
                //     Debug.Log("acelerando");
                // persecucion del protagonista
                if (puede_moverse == true)
                {
                    // para establecer la ruta, resta las coordenadas del punto final menos las de la posicion actual
                    float xComp = Jugador.transform.position.x - rb.transform.position.x;
                    if (xComp > max_velocidad)
                    {
                        xComp = max_velocidad;
                    }
                    //float yComp = Jugador.transform.position.y - rb.transform.position.y;
                    float yComp = rb.transform.position.y;
                    Vector2 direccion = new Vector2(xComp, yComp);          // crea la direccion


                    /*if (direccion.magnitude <= vision)                  // verifica si el protagonista esta en su linea de vision
                    {

                    }*/
                    if (rb.velocity.magnitude < 10)              // mientras pueda acelerar, acelera
                    {
                        rb.AddForce(direccion * velocidad);                // multiplicar por la velocidad potencia la fuerza de impulso
                    }

                }
            }
            if (rb.velocity.magnitude > max_velocidad)
            {
                //  Debug.Log("frenando");
                Vector2 freno = new Vector2(-rb.velocity.x, rb.velocity.y);
                rb.AddForce(freno);

            }
        }
	}

    /*************************************************************
                FUNCIONES DE USUARIO
    *************************************************************/


    /**
     * setPlayer
     * establece una referencia al objeto jugador
     */
     public void setPlayer(GameObject jugador)
    {
        Jugador = jugador;
    }

    /** 
     * Kill
     * Metodo de la interfaz IKillable
     * Destruye un objeto
     */
    public void Kill()
    {
        // aqui pondriamos transiciones a "muriendo" y operaciones antes de destruir el 
        Destroy(this.gameObject);
    }

    /**
     * Damage
     * metodo de la interfaz IDamageable
     */
    public void Damage(float damageTaken)
    {
        
        vidaCurr = vidaCurr - damageTaken;                          // resta de la vida actual
        //img.color = Color.Lerp(rojo, verde, vidaCurr/ vidaMax);     // divide la barra de vida en la porcion correspondiente y la pinta
        barraVida.value = vidaCurr / 100;                           // asigna el valor a la barra de vida
        // comprueba si sigue vivo
        if (vidaCurr <= 0)
        {
            // hay que matarlo
            Kill();
        }
    }

    /**
     * Se activa cuando entra en contacto con otro objeto
     * Cuando el enemigo entra en contacto con el jugador, le hace daño
     * NOTA: para que funcione, el collider debe tener marcada la casilla de "is trigger".
     */
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {

            DealDamage(poder, other.gameObject);        // realiza el daño a traves de la interfaz
        }
        else
        {

        }
    }

    /**
     * DealDamage
     * Metodo de la interfaz IDamager
     * Este metodo se sobreescribe para realizar el daño segun personaje
     */
    public void DealDamage(float damageDealt, GameObject other)
    {
        if (other.GetComponent<movimiento_principal>() is IDamageable<float>)       // comprueba que el objeto se puede dañar
        {
            other.GetComponent<movimiento_principal>().Damage(poder);               // le hace el daño
        }
    }




    // Se ejecuta siempre que el objeto entra en contacto con un collider por primera vez.
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Plataforma")      // comprueba que el otro objeto tiene la etiqueta Plataforma
        {
            puede_moverse = true;                    // el personaje puede saltar
        }
        if ((col.gameObject.tag == "Player")&&(Time.time > tiempoAtaque))
        {
            tiempoAtaque = Time.time + cadencia;
           
            DealDamage(poder, col.gameObject);        // realiza el daño a traves de la interfaz
        }
        else
        {
        
        }
    }

    // Se ejecuta siempre que el objeto permanezca en contacto con un collider. Puede ejecutarse muchas veces.
    void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.tag == "Plataforma")
        {
            puede_moverse = true;
        }
    }

    // Se ejecuta siempre que el objeto deje de estar en contacto con un collider.
    void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.tag == "Plataforma")
        {
            puede_moverse = false;
        }

    }

}
