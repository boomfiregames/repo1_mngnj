﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {
    GameObject[] pauseObjects;
    public string nextSceneName;                // nombre de la proxima escena


    // Use this for initialization
    void Start()
    {
        Time.timeScale = 1;
        pauseObjects = GameObject.FindGameObjectsWithTag("onPause");
        hidePaused();
    }

    // Update is called once per frame
    void Update()
    {

        //uses the p button to pause and unpause the game
        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Log("stop!");
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;                 
                showPaused();
            }
            else if (Time.timeScale == 0)
            {
                Debug.Log("high");
                Time.timeScale = 1;
                hidePaused();
            }
        }
    }


    public void onHotStart()
    {
        pauseObjects = GameObject.FindGameObjectsWithTag("onPause");
    }

    //Reloads the Level
    public void Reload()
    {
        SceneManager.LoadScene("prueba");
    }

    //controls the pausing of the scene
    public void pauseControl()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
            showPaused();
        }
        else if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
            hidePaused();
        }
    }

    //shows objects with ShowOnPause tag
    public void showPaused()
    {
        if(pauseObjects == null)
        {
            onHotStart();           // recupera los elementos
        }
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(true);
        }
    }

    //hides objects with ShowOnPause tag
    public void hidePaused()
    {
        if (pauseObjects == null)
        {
            onHotStart();           // recupera los elementos
        }
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(false);
        }
    }


    //loads inputted level
    public void CargarNivel(string level)
    {
        SceneManager.LoadScene(level);
    }


    public void Salir()
    {
       
        Application.Quit();             // esta funcion se ignora en el editor: solo se puede probar tras exportar el juego
    }
}
