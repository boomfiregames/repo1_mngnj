﻿using UnityEngine;
using System.Collections;

public class enemyBullet : MonoBehaviour {

    public Vector3 dir_lanzamiento;
    public float fuerza_lanzamiento = 25f;
    public int pupa = 1;
    public float velocidad = 10f;
    public float tiempo_limite = 8f;
    //public bool rebota = false;
    public bool se_divide = false;
    public GameObject division1;
    public GameObject division2;
    public GameObject division3;
    public int n_divisiones = 4;
    public int extra = 0;
    
	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody2D>().AddForce(new Vector3(dir_lanzamiento.x, dir_lanzamiento.y, 0) * fuerza_lanzamiento, ForceMode2D.Impulse);
        //Debug.Log("Tirada ");
        tiempo_limite = tiempo_limite + Time.time;
    }
	
	// Update is called once per frame
	void Update () {
        
	    if(tiempo_limite < Time.time)
        {
            if (se_divide)
            {
                GameObject go;
                for (int i = 0; i < n_divisiones; i++)
                {
                    switch (Random.Range(1, 3))
                    {
                        case 1:
                            go = (GameObject)Instantiate(division1, GetComponent<Transform>().position, Quaternion.identity);
                            break;
                        case 2:
                            go = (GameObject)Instantiate(division2, GetComponent<Transform>().position, Quaternion.identity);
                            break;
                        case 3:
                            go = (GameObject)Instantiate(division3, GetComponent<Transform>().position, Quaternion.identity);
                            break;
                        default:
                            go = (GameObject)Instantiate(division1, GetComponent<Transform>().position, Quaternion.identity);
                            break;

                    }
                    switch (i % 4)
                    {
                        case 0:
                            go.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.right*velocidad, ForceMode2D.Force);
                            break;
                        case 1:
                            go.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.right * velocidad * (-1), ForceMode2D.Force);
                            break;
                        case 2:
                            go.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * velocidad, ForceMode2D.Force);
                            break;
                        case 3:
                            go.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * velocidad * (-1), ForceMode2D.Force);
                            break;
                    }
                }
            }
            Debug.Log("EnemyBullet : expirando...");
            Destroy(this.gameObject);
        }
	}


    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("EnemyBullet : player...");

            //other.gameObject.GetComponent<protagonista>().recibir(pupa+extra, GetComponent<Rigidbody2D>().velocity);
            Destroy(this.gameObject);
        }
        if(other.gameObject.tag == "Environment")
        {
            Debug.Log("EnemyBullet : muro...");

            Destroy(this.gameObject);
        }
    }
}
