﻿using UnityEngine;
using System.Collections;

public class movimmiento : MonoBehaviour {

    private bool puede_saltar;
    // variables de recogida de teclado
    private KeyCode saltar = KeyCode.W;
    private KeyCode derecha = KeyCode.D;
    private KeyCode izquierda = KeyCode.A;
    private KeyCode bajar = KeyCode.S;

    public Rigidbody2D rb;
    /**********************************************************/
    // Use this for initialization
    void Start() {
        puede_saltar = false;
        // obtener rigidbody del gameobject
        rb = gameObject.GetComponent<Rigidbody2D>();
    }
    /**********************************************************/
    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(derecha))
        {
            // se cumple
            rb.velocity = transform.right * 10;
        }

        if (Input.GetKeyDown(izquierda))
        {
            // se cumple
            rb.velocity = transform.right * -10;
        }

        if (Input.GetKeyDown(saltar))
        {
            if (puede_saltar == true)
            {
                rb.velocity = transform.up * 10;
                puede_saltar = false;
            }
        }
        /*
        if (Input.GetKeyDown(derecha))
        {
            // se cumple
            rb.velocity = transform.right * 10;
        }*/


    }
    /**********************************************************/
    void OnCollisionEnter2D(){
        puede_saltar = true;
    }
    /*
    void OnCollisionStay2D()
    {
        puede_saltar = true;
    }
    */
    void OnCollisionExit2D()
    {
        puede_saltar = false;
    }
    /**********************************************************/

}
