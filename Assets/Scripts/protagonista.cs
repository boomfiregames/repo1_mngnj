﻿using UnityEngine;
using System.Collections;

public class protagonista : MonoBehaviour {

    // grav scale = 1, mass = 20
    public float velocidad = 500;
    public float salto = 200;

    public float maxVel = 100;
    public float velPararse = 3f;
    public float curVel = 0f;
    public float rozamiento = 1.1f;

    public float t_ataque = 0.5f;
    public float siguiente_ataque = 0f;

//    public GameObject UI;

    public float fuerza_retroceso = 3;
    public int mirando = 1;                 // hacia donde mira el prota: 1 = derecha, -1 = izquierda         
    public bool puede_saltar = false;

    public GameObject balas;                // tipo de proyectil que lanza

    KeyCode arriba = KeyCode.UpArrow;
    KeyCode izquierda = KeyCode.LeftArrow;
    KeyCode derecha = KeyCode.RightArrow;
    KeyCode abajo = KeyCode.DownArrow;

    KeyCode ir_derecha = KeyCode.D;
    KeyCode ir_izquierda = KeyCode.A;
    // KeyCode agacharse = KeyCode.S;
    KeyCode saltar = KeyCode.W;

    private float ymax = -10;
    private bool stop = false;
    
    private Rigidbody2D rb;
    private SpriteRenderer sr;
    private GameObject arma;

    public int sentido = 1;

    public float taux = -1;
    
    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
	}


    void Update()
    {
        if (Input.GetKey(ir_derecha))
        {
            if (GetComponent<Rigidbody2D>().velocity.magnitude > maxVel)
            {
                // avanza a max vel
                // rb.velocity = new Vector2(maxVel, rb.velocity.y);
            }
            else
            {
                // aumenta hasta llegar a max vel
                rb.AddForce(transform.right * velocidad, ForceMode2D.Force);
                // rb.velocity = new Vector2(maxVel, rb.velocity.y);
                // rb.MovePosition(rb.position + (Vector2)(transform.right * velocidad * Time.fixedDeltaTime));
            }
            sentido = 1;
        }
        else
        {
            if (Input.GetKey(ir_izquierda))
            {
                if (GetComponent<Rigidbody2D>().velocity.magnitude > maxVel)
                {
                    // avanza a max vel
                    //rb.velocity = new Vector2(-maxVel, rb.velocity.y);
                }
                else
                {
                    // aumenta hasta llegar a max vel
                    rb.AddForce(-transform.right * velocidad, ForceMode2D.Force);
                    // rb.velocity = new Vector2(- maxVel, rb.velocity.y);
                    //rb.MovePosition(rb.position - (Vector2)(transform.right * velocidad * Time.fixedDeltaTime));
                }
            }
            sentido = -1;
        }

        // SALTO
        if (Input.GetKeyDown(saltar) && isGrounded())
        {
            rb.AddForce(transform.up * salto, ForceMode2D.Impulse);   
        }

        // DISPARO
        if (Input.GetKey(derecha))
        {
            Disparar(transform.right, 1);
        }
        else {
            if (Input.GetKey(izquierda))
            {
                Disparar(transform.right, -1);
            }
            else
            {
                if (Input.GetKey(arriba))
                {
                    Disparar(transform.up, 1);
                }
                else
                {
                    if (Input.GetKey(abajo))
                    {
                        Disparar(transform.up, -1);
                    }
                }
            }
        }
        // si no hay mas input de movimiento, pero si inercia y esta en contacto con el suelo
        if (!want2Move() && rb.velocity.magnitude != 0 && isGrounded())
        {
            parar();
        }
    }

    /**
     * Corrige la trayectoria cuando la velocidad es muy alta    
     */
    public void lightSpeedFix()
    {
        RaycastHit2D[] impactos = new RaycastHit2D[2];
        Vector2 normal;
        float seno;
        float radianes, grados;
        // lanza rayo
        if (rb.Cast(rb.velocity, impactos, rb.velocity.magnitude) > 0)
        {
            // obtiene puntos de impacto
            normal = impactos[0].normal;
            // obtiene normal y angulo de abordaje respecto al collider
            seno = rb.velocity.y / rb.velocity.magnitude;
            radianes = Mathf.Asin(seno);
            grados = radianes * Mathf.Rad2Deg;
            if (grados > 45)    // REBOTE
            {

            }
            else                // DESLIZAMIENTO
            {
                
            }
        }
        else
        {
            Debug.Log("No hay colisiones");
        }
    }

    // Update is called once per frame
   // void FixedUpdate(){


        /*
        if (transform.position.y > ymax)
        {
            ymax = transform.position.y;
        }
        if (GetComponent<Rigidbody2D>().velocity.magnitude == 0 && !stop)
        {
            //Debug.Log("y = "+ymax);
            stop = true;
        }


        // MOVIMIENTO
        // derecha
        //Debug.Log("velocidad = "+GetComponent<Rigidbody2D>().velocity.magnitude);
        if (Input.GetKey(ir_derecha))
        {
            if (GetComponent<Rigidbody2D>().velocity.magnitude > maxVel)
            {
                // max vel derecha
                Debug.Log("max vel derecha");
                GetComponent<Rigidbody2D>().velocity = transform.right * velocidad;
            }
            else
            {
                Debug.Log("vel++ derecha" + GetComponent<Rigidbody2D>().velocity);
                // menor que max vel derecha
                rb.AddForce(transform.right * velocidad, ForceMode2D.Force);
            }
            sentido = 1;
        }
        else
        {
            if (Input.GetKey(ir_izquierda))
            {
                if(GetComponent<Rigidbody2D>().velocity.magnitude > maxVel)
                {
                    Debug.Log("max vel izda");
                    // max vel izda
                    GetComponent<Rigidbody2D>().velocity = transform.right * velocidad;
                }
                else
                {
                    Debug.Log("vel++ izda: "+GetComponent<Rigidbody2D>().velocity);
                    // menor que max vel izquierda
                    rb.AddForce(transform.right * velocidad * -1, ForceMode2D.Force);
                }
                sentido = -1;
            }
        }
        
        // SALTO
        if (Input.GetKeyDown(saltar) && puede_saltar)
        {
            stop = false;
            
            rb.AddForce(transform.up * salto, ForceMode2D.Impulse); //631.328
            // rb.AddForce(transform.up * salto, ForceMode2D.Force);

        }

        // DISPARO
        if (Input.GetKey(derecha))
        {
            sentido = 1;
            Disparar(transform.right, 1);
        }
        else {
            if (Input.GetKey(izquierda))
            {
                sentido = -1;
                Disparar(transform.right, -1);
            }
            else
            {
                if (Input.GetKey(arriba))
                {
                    sentido = 1;
                    Disparar(transform.up, 1);
                }
                else
                {
                    if (Input.GetKey(abajo))
                    {
                        sentido = -1;
                        Disparar(transform.up, -1);
                    }
                }
            }
        }

        if ( (Input.GetKeyUp(ir_derecha) || Input.GetKeyUp(ir_izquierda)) && GetComponent<Rigidbody2D>().velocity.magnitude != 0) 
        {
            parar();
        }
        /*
        if((Time.time - taux == 3) && taux > 0)
        {
            Debug.Log("y = " + transform.position.y);
        }*/
        /*
        if(!Input.anyKey && GetComponent<Rigidbody2D>().velocity.magnitude > 0)
        {
            curVel = GetComponent<Rigidbody2D>().velocity.y;
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x / 2, curVel);
        }*/
    //}


   

    /**
    */
    public bool want2Move()
    {
        // si no tiene input de movimiento 
        if (!Input.GetKey(ir_derecha) && !Input.GetKey(ir_izquierda))
        {
            return false;
        }
        return true;
    }


    /****/
    public void parar()
    {
        Vector2 currVel = rb.velocity;
        if(Mathf.Abs(currVel.x) > 0)
        {
            rb.velocity = new Vector2(currVel.x / rozamiento, currVel.y);
        }

    }

    /****/
    public bool isGrounded()
    {
        RaycastHit2D[] hit = new RaycastHit2D[2];
        if (rb.Cast(-transform.up, hit, 1f) > 0)
        {
            if (hit[0].collider != null)
            {
                if (hit[0].collider.gameObject.tag == "Player")
                {
                    Debug.Log("FAIL");
                }
                
                return true;
            }
            
            return false;
        }
        return false;
        
    }

    /*************************************/
    // entra en contacto
    public void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "Environment" || col.gameObject.tag == "Enemy")
        {
            puede_saltar = true;
        }
    }

    public void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.tag == "Environment" || col.gameObject.tag == "Enemy")
        {
            puede_saltar = true;
        }
    }

    public void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.tag == "Environment" || col.gameObject.tag == "Enemy")
        {
            puede_saltar = false;
            
        }
    }
    /*************************************/
    /**
     * Efectua un disparo
     * @arg direccion : linea que define la direccion de la trayectoria. Debe ser vector unario.
     * @arg sentido : determina el sentido del prota. 1 = derecha, -1 = izquierda --- PUEDE SER REDUNDANTE
     */
    public void Disparar(Vector2 direccion, int sent)
    {
        if (Time.time > siguiente_ataque) {
            GameObject pro = (GameObject)Instantiate(balas, transform.position, Quaternion.identity);
            pro.GetComponent<bala>().direccion = direccion;
            pro.GetComponent<bala>().sentido = sent;
            aplicarRetroceso(direccion, sent);

            siguiente_ataque = Time.time + t_ataque;
        }
    }

    /**
     * Aplica un ligero impulso en la misma direccion de disparo pero sentido contrario
     * @arg direccion: linea que define la direccion de la trayectoria. Debe ser vector unario.
     */
    public void aplicarRetroceso(Vector2 direccion, int sent)
    {
        rb.AddForce(direccion * (-sent) *fuerza_retroceso, ForceMode2D.Impulse);
    }

    /// <summary>
    /// 
    /// </summary>
    public void morir()
    {
       // Destroy(this.gameObject);
    }
 
}
