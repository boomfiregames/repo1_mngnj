﻿using UnityEngine;
using System.Collections;

public class bala : MonoBehaviour {
    // general
    public int pupa = 1;
    public float velocidad = 1f;
    // direccion lineal + extra dmg (revisar esto ultimo)
    public Vector2 direccion;
    public int sentido;
    public int extra = 0;
    // variables tiro perdigonada
    public float velx = 0.1f;
    public float vely = 0.1f;

    
    
    // Use this for initialization
    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = direccion * sentido * velocidad;
        if (Mathf.Abs(direccion.x) < Mathf.Abs(direccion.y))
        {
            GetComponent<Transform>().Rotate(0, 0, 90);
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            //Debug.Log("Procurando pupita");
            other.gameObject.GetComponent<enemigo>().recibir(pupa+extra);
            Destroy(this.gameObject);
        }
        if(other.gameObject.tag == "Environment")
        {
            
            Destroy(this.gameObject);
        }
    }

}

