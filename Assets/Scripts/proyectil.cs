﻿using UnityEngine;
using System.Collections;

public class proyectil : MonoBehaviour {

    public Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();

        rb.velocity = transform.right * 10;
        GetComponent<Transform>().Rotate(0, 0, 90);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    // 
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag != "Player")
        {
            hacerPupa(other.gameObject);
            Debug.Log("me acabo de chocar con : " + other.gameObject.name);
            Destroy(this.gameObject);
        }
    }

    // esto hace daño
    void hacerPupa(GameObject other)
    {
        other.GetComponent<hacerPupa>().recibir(50);
    }
}
