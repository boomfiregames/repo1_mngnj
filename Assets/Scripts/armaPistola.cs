﻿using UnityEngine;
using System.Collections;

public class armaPistola : MonoBehaviour
{

    // proyectiles que admite
    public GameObject proy0;
    public GameObject proy1;
    public GameObject proy2;
    public int tipo_proy = 0;       // 0, 1 y 2

    public int nproy = 5;           // perdigones por disparo

    public float xDispersion = 0.5f;
    public float yDispersion = 0.5f;
    public float potencia = 2f;
    public GameObject aux;


    // Use this for initialization
    void Start()
    {

    }

    /***/
    public void setProy(int id)
    {
        tipo_proy = id;
        if (tipo_proy < 0 || tipo_proy > 3)
        {
            tipo_proy = 0;
        }
    }

    /***/
    public void nextProy()
    {
        tipo_proy = (tipo_proy + 1) % 3;
    }


    public GameObject getProyectil()
    {
        switch (tipo_proy)
        {
            case 0:
                return proy0;
                break;
            case 1:
                return proy1;
                break;
            case 2:
                return proy2;
                break;
            default:
                return proy0;
                break;
        }
    }

    // crea un solo proyectil y le otorga movimiento
    // orientacion : true -> derecha, false -> izquierda
    public void shoot(Vector2 dir, int sent, bool orientacion)
    {
        Transform spawnPoint;

        if (orientacion)
        {
           spawnPoint = transform.GetChild(0).GetComponent<Transform>();
        }
        else
        {
            spawnPoint = transform.GetChild(1).GetComponent<Transform>();
        }

        /*
        if(sentido > 0)
        {
            spawn.x = spawn.x * -1;
        }
        */
        GameObject proy = (GameObject)Instantiate(getProyectil(), spawnPoint.position, Quaternion.identity);
        proy.GetComponent<bala>().sentido = sent;
        proy.GetComponent<bala>().direccion = dir * potencia;
        // proy.GetComponent<bala>().extra = extra;
        proy.GetComponent<bala>().extra = 0;

    }

}
