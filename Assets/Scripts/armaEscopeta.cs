﻿using UnityEngine;
using System.Collections;

public class armaEscopeta : MonoBehaviour {

    // proyectiles que admite
    public GameObject proy0;
    public GameObject proy1;
    public GameObject proy2;
    public int tipo_proy = 0;       // 0, 1 y 2

    public int nproy = 5;           // perdigones por disparo

    public float xDispersion = 0.5f;
    public float yDispersion = 0.5f;
    public float potencia = 2f;
    public GameObject aux;

    public Transform spawn_der;
    public Transform spawn_izq;

    // Use this for initialization
    void Start () {
	
	}
	

    public void setProy(int id)
    {
        tipo_proy = id;
        if(tipo_proy < 0 || tipo_proy > 3)
        {
            tipo_proy = 0;
        }
    }

    public void nextProy()
    {
        tipo_proy = (tipo_proy + 1) % 3;
    }

    // crea varios proyectiles en un mismo punto y los lanza hacia una direccion con dispersion
    public void shoot(Vector2 dir, int sent, bool orientacion)
    {
        Transform spawn;
        Vector2 trayectoria;
        float velx;
        float vely;
        bool vertical = false;
        if(dir.x < dir.y)
        {
            vertical = true;
        }
        for (int i = 0; i < nproy; i++)
        {
            if (vertical)
            {
                velx = 1;
                //vely = potencia;
                vely = 1;       // test
                trayectoria = new Vector2(dir.x + Random.Range(-xDispersion, xDispersion), dir.y);
            }
            else
            {
                vely = 1;
                //velx = potencia;
                velx = 1;       // test
                trayectoria = new Vector2(dir.x, dir.y + Random.Range(-yDispersion, yDispersion));
            }
            if(orientacion)
            {
                spawn = spawn_der;
            }
            else
            {
                spawn = spawn_izq;
            }
            aux = (GameObject)Instantiate(proy0, spawn.position, Quaternion.identity);
            //aux.GetComponent<bala>().direccion = trayectoria;
            aux.GetComponent<bala>().direccion = dir;
            aux.GetComponent<bala>().sentido = sent;
            aux.GetComponent<bala>().velx = velx;
            aux.GetComponent<bala>().vely = vely;

        }
    }
}
