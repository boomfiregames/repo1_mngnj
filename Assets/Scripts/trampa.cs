﻿using UnityEngine;
using System.Collections;


/**
* Representa una trampa.
* Los tipos de trampa van a ser:
* - Paredes de pinchos: indestructible. Pueden estar ocultas y salir. Si se cuenta con la maldicion de Huir, la pared persigue.
* - Cajas voladoras: pueden seguir un patron de movimiento o ser fijas.
* - Bombas ocultas: al seleccionar la maldicion de las trampas, el perk asociado siempre sera "hablar"(muestra dialogos informativos, uno de ellos avisa de bomba).
* - Torretas: arrojan proyectiles al jugador. Fijas o moviles, vulnerables e indestructibles.
*/
public class trampa : MonoBehaviour {

    public bool caja_voladora = false;
    public bool bomba = false;
    public bool torreta = false;
    public bool pinchos = false;

    // caja
    public float x = 2;         // desplazamiento max x
    public float y = 2;         // desplazamiento max y
    public int estado = 0;      // estado de desplazamiento en el que se encuentra
    public float t_desplazamiento = 3;
    public float fin_desplazamiento = 0;
    public float velocidad = 300;     // en realidad vel = x / t_desplazamiento
    // general
    private Rigidbody2D rb;
    public int pupa = 2;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        if (caja_voladora)
        {
            volar();
        }
	}

    public void volar(/*int patron*/)
    {
        if (Time.time >= fin_desplazamiento)
        {
            // op 1
            //rb.isKinematic = true;
           // rb.isKinematic = false;
            // op 2
             rb.velocity = new Vector2(0,0);
            //   switch(patron){posibles patrones de movimiento}
            //  en un primer momento solo haremos el patron cuadrado, y metiendo 0 a una de sus componentes se obtiene el movimiento lineal simple
            switch (estado)
            {
                case 0:
                    // esquina inferior derecha -> esquina inferior izquierda
                    rb.AddForce(transform.right * velocidad * (-1), ForceMode2D.Force);
                    break;
                case 1:
                    // esquina inferior izquierda -> esquina superior izquierda
                    rb.AddForce(transform.up * velocidad, ForceMode2D.Force);
                    break;
                case 2:
                    // esquina superior izquierda -> esquina superior derecha
                    rb.AddForce(transform.right * velocidad, ForceMode2D.Force);
                    break;
                case 3:
                    // esquina superior derecha -> esquina inferior derecha
                    rb.AddForce(transform.up * velocidad * (-1), ForceMode2D.Force);
                    break;
            }
            estado = (estado + 1) % 4;
            fin_desplazamiento = Time.time + t_desplazamiento;
        }

    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
       //     other.gameObject.GetComponent<protagonista>().recibir(pupa, getDireccion(rb.velocity));
        }
    }



    public Vector2 getDireccion(Vector2 fatDir)
    {
        // obtenemos distancia grande
        Vector2 aux = fatDir;
        Vector2 direccion = new Vector2(aux.x / aux.magnitude, aux.y / aux.magnitude);
        return direccion;
    }
}
