﻿using UnityEngine;
using System.Collections;
/* ESTA CLASE CONTIENE TODOS LOS METODOS DE TODOS LOS ENEMIGOS */
public class enemigo : MonoBehaviour {

    /****
     Listado enemigos:
        - Triangulito: se mueve lateralmente, al chocar con la pared cambia el sentido. Daña al tocar.
        - Rombo : Persigue al jugador. Puede saltar. Daña al tocar.
        - Estrella : Se  mueve aleatoriamente por el aire. Cuando el jugador esta cerca lo persigue. Daña tirando cosas.
        - Bolitas : Se mueven saltando. Se dividen al morir. Persiguen al jugador. Daña al tocar.
        - Bola : Se mueve lentamente. Puede saltar. Se divide en Bolitas. Movimiento lateral, cambia sentido al tocar pared. Daña al tocar.
        - Cuadrado : Se mueve aun mas lentamente. Tira bolitas. Al morir explota en Bolas y Bolitas. Movimiento lateral, persiguiendo al jugador. Daña al tocar.
    
    Cada enemigo tiene una configuracion de acciones. Las acciones son:
        1. movimiento por defecto.
        2. daño por contacto.
        3. perseguir al jugador.
        4. saltar.
        5. movimiento aleatorio por el aire.
        6. tirar cosas.
        7. dividirse al morir.
        8. tirar bolitas.
        9. soltar premios.
        10. experiencia o algo de ese palo.
        11. soltar enemigos.
        12. soltar balas especiales.
        13. soltar enemigos al morir.
        14. suelta bolitas al morir.
        15. arroja enemigos.

    Las configuraciones son arrays de booleanos
     ****/

    // PRINCIPAL
    public bool movimiento_horizontal = false;
    public bool pupa_contacto = false;
    public bool perseguir = false;
    public bool saltar = false;
    public bool vuela = false;
    public bool tira_cosas = false;
    public bool premios_ondead = false;
    public bool enemigos = false;
    public bool balas_especiales = false;
    public bool tira_bolitas = false;
    public bool enemigo_ondead = false;
    public bool tira_bolitas_ondead = false;
    public bool tira_enemigos = false;
    public bool disparo_circular = false;


    public Vector2 dir_lanzamiento;

    
    public GameObject drop1;
    public GameObject drop2;
    public GameObject drop3;
    public GameObject specialDrop;
    // MOVIMIENTO
    public float max_vel = 10f;
    public int sentido = -1;
    public float velocidad = 1f;
    public bool puede_caminar = false;
    public int max_hp = 2;
    public int curr_hp = 2;
    
    // PUPA
    public int fuerza = 1;
    public int extra = 0;

    // VOLAR
    public float fuerza_impulso = 2f;
    public float tiempo_impulso = 3f;
    public float siguiente_impulso = 0f;
    public float rango_vision = 20f;
    private Rigidbody2D rb;

    // TIRAR
    public bool HQ = false;
    public GameObject bujio;
    public GameObject cosa1;        // bolita por defecto
    public GameObject cosa2;
    public GameObject cosa3;
    public GameObject cosa4;        // bala especial por defecto
    public GameObject enemigo1;
    public GameObject enemigo2;
    public GameObject enemigo3;     // enemigos 3 y 4 por si dropeara enemigos al morir
    public GameObject enemigo4;
    public float fuerza_lanzamiento = 2f;
    public float max_x = 2f;
    public float max_y = 2f;
    public int n_cosas = 3;
    public int n_enemigos = 2;
    public float siguiente_lanzamiento = 0f;
    public float tiempo_ataque = 3f;
    public float siguiente_lanzamiento_enem = 0f;
    public float tiempo_lanz_enem = 10f;

    // SALTO
    public float fuerza_salto = 3f;
    public float retardo_salto = 2f;
    public float siguiente_salto = 0f;
    // Use this for initialization
    void Start () {
       
        rb = GetComponent<Rigidbody2D>();
        if(dir_lanzamiento != null)
        {
            rb.AddForce(dir_lanzamiento, ForceMode2D.Force);
        }
	}
	
	// Update is called once per frame
	void Update () {

        /********************************************************/
        if (movimiento_horizontal)
        {
            movimientoLateral();        // actualizar con perseguir
        }
        if (saltar)
        {
            propuSalto();
        }
        if (vuela)
        {
            if(Time.time == tiempo_impulso - 2)
            {
                rb.isKinematic = true;
                rb.isKinematic = false;
            }
            volar();
        }
        if (tira_cosas)
        {
            tirarCosas(false, false);
        }
        if (enemigos)
        {
            tirarEnemigos(false, false);
        }
        if (balas_especiales)
        {
            tirarCosas(false, disparo_circular);
        }
        if (tira_bolitas)
        {
            disparoDisperso(n_cosas, true);

        }
        if (tira_enemigos)
        {
            tirarEnemigos(true, false);
        }


    /********************************************************/

}


    public GameObject getPlayer()
    {
        return GameObject.FindGameObjectWithTag("Player");
    }


    public void incrementarSalud()
    {
        max_hp++;
    }
    public void incrementarPupa()
    {
        extra++;
    }
    public void propuSalto()
    {
        if(siguiente_salto < Time.time && puede_caminar)
        {
            if (perseguir)
            {
                rb.AddForce(new Vector2((getPlayer().GetComponent<Transform>().position.x - GetComponent<Transform>().position.x) / Mathf.Abs(getPlayer().GetComponent<Transform>().position.x - GetComponent<Transform>().position.x), (getPlayer().GetComponent<Transform>().position.y - GetComponent<Transform>().position.y) / Mathf.Abs(getPlayer().GetComponent<Transform>().position.y - GetComponent<Transform>().position.y))*fuerza_salto, ForceMode2D.Force);
            }
            else
            {
                rb.AddForce(new Vector2(sentido, 1) *fuerza_salto, ForceMode2D.Force);
            }

            siguiente_salto = Time.time + retardo_salto;
        }
    }
    /// <summary>
    /// ///////////////////////////////////////
    /// </summary>
    /// <param name="tiralo"></param>
    public void tirarEnemigos(bool tiralo, bool ondead)
    {
        if (ondead)
        {
            Debug.Log("Tirando enemigos al morir...");
            GameObject go;
            switch (Random.Range(1, 3))
            {
                case 1:
                    go = (GameObject)Instantiate(enemigo3, bujio.GetComponent<Transform>().position, Quaternion.identity);
                    break;
                case 2:
                    go = (GameObject)Instantiate(enemigo4, bujio.GetComponent<Transform>().position, Quaternion.identity);
                    break;


                default:
                    go = (GameObject)Instantiate(enemigo3, bujio.GetComponent<Transform>().position, Quaternion.identity);
                    break;
            }

            if (tiralo)
            {
                go.GetComponent<enemigo>().dir_lanzamiento = getDireccion(getDistancia());
                  
            }
            go.GetComponent<enemigo>().extra = extra;
            go.GetComponent<enemigo>().max_hp = go.GetComponent<enemigo>().max_hp + extra;
            go.GetComponent<enemigo>().curr_hp = go.GetComponent<enemigo>().curr_hp + extra;

        }
        else {
            if (siguiente_lanzamiento_enem < Time.time)
            {
                

                Transform jtr = getPlayer().GetComponent<Transform>();
                Transform tr = GetComponent<Transform>();
                Vector2 distancia = new Vector2(jtr.position.x - tr.position.x, jtr.position.y - tr.position.y);
                if (distancia.magnitude < rango_vision || HQ)
                {
                    Debug.Log("Tirando enemigos...");
                    GameObject go;
                    GameObject proyectil;
                    Vector2 dir_lanzamiento = getDireccion(getDistancia());
                    for (int i = 0; i < n_cosas; i++)
                    {

                        switch (Random.Range(1, 3))
                        {
                            case 1:
                                proyectil = enemigo1;
                                break;
                            case 2:
                                proyectil = enemigo2;
                                break;


                            default:
                                proyectil = enemigo1;
                                break;
                        }

                        go = (GameObject)Instantiate(proyectil, bujio.GetComponent<Transform>().position, Quaternion.identity);

                        go.GetComponent<enemigo>().extra = extra;
                        //go.GetComponent<enemigo>().dir_lanzamiento = dir_lanzamiento;
                        go.GetComponent<enemigo>().max_hp = go.GetComponent<enemigo>().max_hp + extra;
                        go.GetComponent<enemigo>().curr_hp = go.GetComponent<enemigo>().curr_hp + extra;




                        if (tiralo)
                        {
                            if (perseguir)
                            {
                                go.GetComponent<enemigo>().dir_lanzamiento = new Vector2(dir_lanzamiento.x, dir_lanzamiento.y) * fuerza_lanzamiento;
                            }
                            else
                            {
                                go.GetComponent<enemigo>().dir_lanzamiento = new Vector2(Random.Range(-2, max_x+1), Random.Range(1 - 2, max_y+1)) * fuerza_lanzamiento;
                            }
                        }
                    }
                }
                siguiente_lanzamiento_enem = Time.time + tiempo_lanz_enem;
            }
        }
    }



    // tira cosas y bolitas
    public void tirarCosas(bool bolita, bool especial)
    {
        if (especial && siguiente_lanzamiento_enem< Time.time) // usamos siguiente lanz enem como aux
        {
            Transform jtr = getPlayer().GetComponent<Transform>();
            Transform tr = bujio.GetComponent<Transform>();
            Vector2 distancia = new Vector2(jtr.position.x - tr.position.x, jtr.position.y - tr.position.y);
           // Debug.Log("Distancia especial : x =" +distancia.x+"  y = "+distancia.y+"  magnitud = "+distancia.magnitude);
            GameObject go;
            Vector2 dir_lanzamiento = getDireccion(getDistancia());

            if (bolita)
            {
                go = (GameObject)Instantiate(cosa1, bujio.GetComponent<Transform>().position, Quaternion.identity);
            }
            else
            {
                go = (GameObject)Instantiate(cosa4, bujio.GetComponent<Transform>().position, Quaternion.identity);
            }


            go.GetComponent<enemyBullet>().dir_lanzamiento = dir_lanzamiento;
            go.GetComponent<enemyBullet>().fuerza_lanzamiento = fuerza_lanzamiento;
            go.GetComponent<enemyBullet>().extra = extra;
            //go.GetComponent<enemigo>().jugador = jugador;


            siguiente_lanzamiento_enem = siguiente_lanzamiento_enem+ tiempo_lanz_enem;
        }
        else
        {
            if (siguiente_lanzamiento < Time.time && tira_cosas)
            {
                Transform jtr = getPlayer().GetComponent<Transform>();
                Transform tr = GetComponent<Transform>();
               
               // Debug.Log("Distancia : x =" + distancia.x + "  y = " + distancia.y + "  magnitud = " + distancia.magnitude);
                if (getDistancia().magnitude < rango_vision)
                {
                    GameObject go;
                    GameObject proyectil;
                    Vector2 dir_lanzamiento = getDireccion(getDistancia());
                    //Debug.Log("dir_lanzamiento : x =" + dir_lanzamiento.x + "  y = " + dir_lanzamiento.y + "  magnitud = " + dir_lanzamiento.magnitude);
                    for (int i = 0; i < n_cosas; i++)
                    {
                        if (bolita)
                        {
                            proyectil = cosa1;

                           // Debug.Log("bolita!");
                        }
                        else
                        {
                            switch (Random.Range(1, 5))
                            {
                                case 1:
                                  //  Debug.Log("switch1 : 1");
                                    proyectil = cosa1;
                                    break;
                                case 2:
                                  //  Debug.Log("switch1 : 2");
                                    proyectil = cosa2;
                                    break;
                                case 3:
                                   // Debug.Log("switch1 : 3");
                                    proyectil = cosa3;
                                    break;
                                case 4:
                                 //   Debug.Log("switch1 : 4");
                                    proyectil = cosa4;
                                    break;

                                default:
                                   // Debug.Log("switch1 : def");
                                    proyectil = cosa1;
                                    break;
                            }
                        }
                        go = (GameObject)Instantiate(proyectil, bujio.GetComponent<Transform>().position, Quaternion.identity);
                        if (perseguir)
                        {
                            go.GetComponent<enemyBullet>().dir_lanzamiento = getDireccion(getDistancia());

                        }
                        else
                        {
                            go.GetComponent<enemyBullet>().dir_lanzamiento = new Vector2(Random.Range(-2, max_x+1), Random.Range(-2, max_y+1));
                           // Debug.Log("dir_lanzamiento : x =" + dir_lanzamiento.x + "  y = " + dir_lanzamiento.y + "  magnitud = " + dir_lanzamiento.magnitude);
                        }
                        
                        go.GetComponent<enemyBullet>().fuerza_lanzamiento = fuerza_lanzamiento;
                        go.GetComponent<enemyBullet>().extra = extra;

                    }
                    siguiente_lanzamiento = Time.time + tiempo_ataque;
                }
            }
        }
    }


    public Vector2 getDireccion(Vector2 fatDir)
    {
        // obtenemos distancia grande
        Vector2 aux = fatDir;
        Vector2 direccion = new Vector2( aux.x / aux.magnitude, aux.y / aux.magnitude);
        return direccion;
    }
    
    public Vector2 getDistancia()
    {
        Vector2 dist = new Vector2(getPlayer().GetComponent<Transform>().position.x - bujio.GetComponent<Transform>().position.x, getPlayer().GetComponent<Transform>().position.y - bujio.GetComponent<Transform>().position.y);
        return dist;
    }

    // vuela
    public void volar()
    {
        if(siguiente_impulso < Time.time)
        {
            Transform jtr = getPlayer().GetComponent<Transform>();
            Transform tr = GetComponent<Transform>();
            Vector2 distancia = new Vector2(jtr.position.x - tr.position.x, jtr.position.y - tr.position.y);
            if (perseguir && distancia.magnitude < rango_vision)
            {
                
                    rb.AddForce(new Vector3(distancia.x / Mathf.Abs(distancia.x), distancia.y / Mathf.Abs(distancia.y))*(fuerza_impulso/20), ForceMode2D.Impulse);
                
            }
            else
            {
                Vector2 vec = new Vector2(Random.Range(0, 2), Random.Range(-2, 2));
                int rnd = Random.Range(-1, 1);
                if(rnd == 0)
                { rnd++; }
                rb.AddForce(vec * fuerza_impulso * sentido * rnd, ForceMode2D.Force);
            }
            
            siguiente_impulso = Time.time + tiempo_impulso;
        }
    }


    // se mueve lateralmente hasta que encuentra una pared, entonces cambia de sentido
    public void movimientoLateral()
    {
        if(rb.velocity.magnitude < max_vel)
        {
            if (perseguir) {
                float xComp = (getPlayer().GetComponent<Transform>().position.x - GetComponent<Transform>().position.x) / Mathf.Abs(getPlayer().GetComponent<Transform>().position.x - GetComponent<Transform>().position.x);
                float yComp = 0;
                Vector2 vec = new Vector2(xComp,yComp);
                rb.AddForce(vec * velocidad , ForceMode2D.Force);
            }
            else {
                rb.AddForce(transform.right * velocidad * sentido, ForceMode2D.Force);
            }
        }
    }


    // Gestiona las colisiones
    public void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            // hace pupa al player
            if (pupa_contacto)
            {
               // other.gameObject.GetComponent<protagonista>().recibir(fuerza, getDireccion(getDistancia()));
            }
            sentido = sentido * (-1);
        }
        if(other.gameObject.tag == "Environment")
        {
            tiempo_impulso = Time.time - 1;
            sentido = sentido * (-1);
            puede_caminar = true;

        }

    }

    public void OnCollisionStay2D(Collision2D other)
    {
        if(other.gameObject.tag == "Environment")
        {
            puede_caminar = true;
        }
    }
    /*
    public void perseguirJugador()
    {
        if(jugador != null)
        {
            Transform tr = GetComponent<Transform>();
            Transform jtr = jugador.GetComponent<Transform>();
            Vector3 distancia = new Vector3(jtr.position.x - tr.position.x, jtr.position.y - tr.position.y, jtr.position.z - tr.position.z);
            if (distancia.magnitude <= rango_vision)
            {

            }
        }   
    }*/

    /************************************************************/
    // FUNCIONES COMUNES
    public void recibir(int fostion)
    {
        curr_hp = curr_hp - fostion;
        if(curr_hp <= 0)
        {
            morir();
        }
    }
    /// <summary>
    /// ////////////////////////////
    /// </summary>
    public void spawnPremiosOnd()
    {
        int tirada = Random.Range(1, 101);
        if (tirada >= 70)
        {
            if (tirada >= 80)
            {
                if (tirada >= 90)
                {
                    if (tirada >= 97)
                    {
                        // spawn premio gordo
                        Instantiate(specialDrop, bujio.GetComponent<Transform>().position, Quaternion.identity);
                    }
                    else
                    {
                        // spawn premio 1
                       Instantiate(drop1, bujio.GetComponent<Transform>().position, Quaternion.identity);
                    }
                }
                else
                {
                    // spawn premio 2
                    Instantiate(drop2, bujio.GetComponent<Transform>().position, Quaternion.identity);
                }
            }
            else
            {
                // spawn premio 3
                Instantiate(drop3, bujio.GetComponent<Transform>().position, Quaternion.identity);
            }
        }
        if (Random.Range(1, 11) > 5)
        {
            // spawn premio 3
            Instantiate(drop3, bujio.GetComponent<Transform>().position, Quaternion.identity);
        }

    }
    /// <summary>
    /// ///////////////////////////////////////
    /// </summary>
    public void morir()
    {
        if (premios_ondead)
        {
            spawnPremiosOnd();
        }
       if (enemigo_ondead) {
            tirarEnemigos(false, tira_enemigos);
        }
        
        if (tira_bolitas_ondead) {
            tirarCosas(true, true);
        }
     

        Destroy(this.gameObject);
    }


    /**             ///POR PROBAR
    * crea n objetos y los dispara en direcciones simetricas
        */
    public void disparoDisperso(int nDisparos, bool circular)
    {
        if (siguiente_lanzamiento < Time.time)
        {
            float parcela;
            float espacio = 100f;                       // 100 unidades de tope de espacio de dispersion
            if (nDisparos != 0)
            {
                parcela = espacio / nDisparos;        // reparte una porcion por disparo
            }
            else
            {
                parcela = -1;
            }
            int currentPortion = 0;                     // determina en que porcion estamos ---> lo determina i
            Vector2 trayectoria = new Vector2(1, 0);    // empezamos en el eje x
            GameObject go;
            if(nDisparos == 0)
            {
                go = (GameObject)Instantiate(getNewProyectil(), bujio.GetComponent<Transform>().position, Quaternion.identity);
                setDisparo(go, trayectoria);

            }
            for (int i = 0; i <= nDisparos; i++)        // se pone asi para 
            {
                Debug.Log("a");

                if ((nDisparos % 2 == 1 && i %2 == 0)||(nDisparos%2 == 0 && i%2 == 1)) {   // si es impar/par ... crea proyectil en los pares/impares respectivamente
                    if (i != 0)
                    {
                        // calcula trayectoria
                        trayectoria.x = trayectoria.x - (parcela * 2 / 100);            // division entre cien para tener un vector direccion chiquito
                        trayectoria.y = trayectoria.y + (parcela * 2 / 100);
                    }
                    // genera proyectil
                    go = (GameObject)Instantiate(getNewProyectil(), bujio.GetComponent<Transform>().position, Quaternion.identity);
                    if (circular)
                    {
                        setDisparo(go, getDireccion(trayectoria));
                    }
                    else
                    {
                        setDisparo(go, trayectoria);
                    }
                    // verifica si es el primero, si no lo es...
                    if (i > 0)
                    {
                        // genera proyectil opuesto : 4º cuadrante
                        go = (GameObject)Instantiate(getNewProyectil(), bujio.GetComponent<Transform>().position, Quaternion.identity);
                        if (circular)
                        {
                            setDisparo(go, getDireccion(new Vector2(trayectoria.x, -trayectoria.y)));
                        }
                        else
                        {
                            setDisparo(go, new Vector2(trayectoria.x, -trayectoria.y));
                        }
                    }
                }

            }
            siguiente_lanzamiento = siguiente_lanzamiento + tiempo_ataque;
        }
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="bala"></param>
    public void setDisparo(GameObject bala, Vector2 dir)
    {
        bala.GetComponent<enemyBullet>().dir_lanzamiento = dir;
        bala.GetComponent<enemyBullet>().fuerza_lanzamiento = fuerza_lanzamiento;
        bala.GetComponent<enemyBullet>().extra = extra;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public GameObject getNewProyectil()
    {
        GameObject proyectil;
        
        switch (Random.Range(1,5))
        {
            case 1:
                proyectil = cosa1;
                break;
            case 2:
                proyectil = cosa2;
                break;
            case 3:
                proyectil = cosa3;
                break;
            case 4:
                proyectil = cosa4;
                break;
            default:
                proyectil = cosa1;
                break;

        }
        return proyectil;
    }





}
