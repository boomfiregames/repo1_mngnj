﻿using UnityEngine;
using System.Collections;

public class coleccionable : MonoBehaviour {

    public float cantidad_restaurada = -50f;
	// Use this for initialization
	void Start () {
	    
	}
	
    public void OnTriggerEnter2D(Collider2D other)
    {
        if((other.gameObject.tag == "Player")&&(other.gameObject.GetComponent<movimiento_principal>() is IDamageable<float>))
        {
            other.gameObject.GetComponent<movimiento_principal>().Damage(cantidad_restaurada);
            Destroy(this.gameObject);
        }
    }
}
