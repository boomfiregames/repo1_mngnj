﻿using UnityEngine;
using System.Collections;

public class enemyHQ : MonoBehaviour {

    public GameObject jugador;              // referencia del jugador
    public GameObject prototipo;            // prototipo del enemigo que no dejara de salir
    public float tiempo_spawn = 5f;              // segundos entre enemigo y enemigo
    public float siguiente_enemigo;         // marca de tiempo para el siguiente enemigo
    public Transform spawn_point;          // ubicacion de salida

    public float rightBound = 38f;          // distancia de el spawn a la derecha
    public float leftBound = 21f;           // distancia de el spawn a la izquierda

    public GameObject copia;
    

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if(Time.time > siguiente_enemigo)
        {

            Debug.Log(">>>>>>>PLOP!");
            siguiente_enemigo = Time.time + tiempo_spawn;
            copia = (GameObject)Instantiate(prototipo, spawn_point.position, Quaternion.identity);
            
            /*
            //randomly moves spawner along x axis
            float x = Random.Range(leftBound, rightBound);
            copia.transform.position = new Vector3(x, this.transform.position.y, 0);
            */
        }
	}
}
