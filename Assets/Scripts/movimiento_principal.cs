﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class movimiento_principal : MonoBehaviour, IDamageable<float>, IKillable
{

    /* DECLARACION DE VARIABLES */

    public GameObject shot;                     // proyectil que vamos a lanzar
    public Transform shtSpwnRight;              // casilla derecha de salida de proyectil
    public Transform shtSpwnLeft;               // casilla izquierda de salida de proyectil
    public Transform shtSpwnUp;                 // NO
    public Transform shtSpwnDown;               // NO

    public int retroceso = 10;                  // ES POSIBLE
    /*
        0 - derecha
        1 - izquierda
        2 - abajo
        3 - arriba
    */
    private int direccion = 0;                  // direccion a la que mira

    //private bool calentamiento = false;       NO

    public GameObject Jugador;          // redundante ?
    public int VELOCIDAD = 20;          // velocidad de movimiento
    public int MAX_VELOCIDAD = 50;      // maxima velocidad
    public int ALTURA = 5;              // propulsion de salto
    public Slider barraVida;            // barra de vida        no olvidar incluir "using UnityEngine.UI;" en la cabecera
    public Image img;                   // imagen de relleno
    public bool puede_saltar;
    private Rigidbody2D rb;             // componente RigidBody2D de este objeto
    public Color verde = Color.green;
    public Color rojo = Color.red;

    // teclas reconocidas
    KeyCode saltar = KeyCode.W;
    KeyCode derecha = KeyCode.D;
    KeyCode izquierda = KeyCode.A;
    KeyCode shootUp = KeyCode.UpArrow;
    KeyCode shootDown = KeyCode.DownArrow;
    KeyCode shootRight = KeyCode.RightArrow;
    KeyCode shootLeft = KeyCode.LeftArrow;


    private float _vidaMax;
    private float _vidaCurr;

    // VARIABLES DE INTERFAZ
    // Este sistema se hace para proteger el acceso a la variable. Suele ser siempre igual.

    public float vidaMax
    {
        get { return _vidaMax; }
        set { _vidaMax = value; }
    }
    ///////
    public float vidaCurr
    {
        get { return _vidaCurr; }
        set { _vidaCurr = value; }
    }
    ///////
    public float tiempoDisparo;         // hara de cronometro
    public float cadencia;              // tiempo que tarda en disparar

    Animator anim;                        // animador

    float speed;                          
    /**
    * Bearing values:
        0 : right
        1 : left
        2 : up
        3 : down
    *
    private int bearing = 0;
        */



    /*************************************************************
        FUNCIONES DE UNITY
    *************************************************************/

    /**
     * Start
     * Esta funcion se ejecuta una sola vez al crear el objeto.
     * Pondremos todas las operaciones de inicializacion aqui.
     */
    void Start()
    {
        /*----------------      NO
        shtSpwnRight = GetComponent<Transform>();
        shtSpwnLeft = shtSpwnRight;
        shtSpwnUp = shtSpwnRight;
        shtSpwnDown = shtSpwnRight;
        ----------------*/
        /* INICIALIZACION */
        anim = GetComponent<Animator>();                // Obtiene el componente Animator, para las transiciones.
        rb = Jugador.GetComponent<Rigidbody2D>();       // Obtiene el componente Rigidbody2D, para el movimiento.
        vidaMax = 100;
        vidaCurr = vidaMax;
    }

    /**
     * Update
     * Esta funcion se ejecuta ciclicamente durante el juego.
     * Desde aqui se controlaran ciertas partes del personaje, como la entrada por teclado.
     */
    void Update()
    {

        /* COMPRUEBA SI SE HA PARADO */
        speed = rb.velocity.magnitude;      // obtiene el valor escalar de la velocidad

        if (speed == 0)
        {
            anim.SetInteger("state", 0);    // si esta quieto, transita de animacion
        }

        // SALTO
        if (Input.GetKeyDown(saltar))
        {
            salto();
        }
        // DESPLAZAMIENTOS
        if (Input.GetKey(derecha))
        {
            desplazamiento(1);
        }
        if (Input.GetKey(izquierda))
        {
            desplazamiento(0);
        }
        /*----------------------------------------
        // SE PARA CUANDO NO HAY MOVIMIENTO
        if ((Input.GetKeyUp(derecha) || Input.GetKeyUp(izquierda)) && (!Input.GetKey(derecha) && !Input.GetKey(izquierda)))
        {
            pararse();
        }
        -----------------------------------------*/
        // DISPARAR
        // aparte de capturar las teclas, revisamos si el personaje puede disparar de nuevo
        if ( (Input.GetKeyDown(shootRight) || Input.GetKey(shootRight) ) && (Time.time > tiempoDisparo) )
        {
            tiempoDisparo = Time.time + cadencia;       // ponemos una "alarma" cuando se pueda disparar de nuevo
            direccion = 0;                              // cambiamos la direccion por la que saldra el proyectil
            disparar();
        }

        if ((Input.GetKey(shootUp) || Input.GetKey(shootUp)) && (Time.time > tiempoDisparo) )
        {
            tiempoDisparo = Time.time + cadencia;
            direccion = 2;
            disparar();
        }
        if ((Input.GetKey(shootDown) || Input.GetKey(shootDown)) && (Time.time > tiempoDisparo))
        {
            tiempoDisparo = Time.time + cadencia;
            direccion = 3;
            disparar();    
        }
        if ((Input.GetKey(shootLeft) || Input.GetKey(shootLeft)) && (Time.time > tiempoDisparo))
        {
            tiempoDisparo = Time.time + cadencia;
            direccion = 1;
            disparar();
        }
    }


    /*************************************************************
                FUNCIONES DE USUARIO
    *************************************************************/

    /**
     * disparar
     * Crea el objeto proyectil rotandolo si es necesario.
    */
    void disparar()
    {
        mover controlador;              // gestiona la direccion
        GameObject proy;                // copia del proyectil
        /*-----------------------
        int dir;                        // direccion de retroceso; 0 = horizontal , 1 = vertical
        int sent;                       // sentido del retroceso
        -------------------------*/
        switch (direccion)
        {
            case 0:                     // derecha
                // copia de proyectil
                proy = (GameObject)Instantiate(shot, shtSpwnRight.position, shtSpwnRight.rotation);     
                controlador = proy.GetComponent<mover>();   // obtiene el componente mover.cs del proyectil
                controlador.direccion = 0;                  // establece parametros del proyectil
                //dir = 0;
                //sent = -1;
                break;
            case 1:                     // izquierda
                proy = (GameObject)Instantiate(shot, shtSpwnLeft.position, shtSpwnLeft.rotation);
                controlador = proy.GetComponent<mover>();
                controlador.direccion = 1;
                //dir = 0;
                //sent = 1;
                break;
            case 2:                     // arriba
                proy = (GameObject)Instantiate(shot, shtSpwnUp.position, shtSpwnUp.rotation);
                controlador = proy.GetComponent<mover>();
                controlador.direccion = 2;
                //dir = 1;
                //sent = -1;
                break;
            case 3:                     // abajo
                proy = (GameObject)Instantiate(shot, shtSpwnDown.position, shtSpwnDown.rotation);
                controlador = proy.GetComponent<mover>();
                controlador.direccion = 3;
                //dir = 1;
                //sent = 1;
                break;
            default:
                //dir = 0;
                //sent = 0;
                break;
        }
        anim.SetInteger("state",3);             // transita a "disparando"
        //aplicarRetroceso(dir, sent);
    }

    /* NO
    dir : 0 = horizontal,  1 = vertical
    sentido : 1 = sentido del eje, -1 = sentido contrario
    */
    void aplicarRetroceso(int dir, int sentido)
    {
        if (dir == 0)
        {
            rb.AddForce(new Vector2(sentido * retroceso, 0), ForceMode2D.Force);
        }else {
            rb.AddForce(new Vector2(0, sentido*retroceso), ForceMode2D.Force);
        }
    }


    /**
     * desplazamiento
     * Gestiona el movimiento del personaje utilizando la fisica.
     * direccion: numero entero que determina hacia donde aplica la fuerza.
    */
    void desplazamiento(int direccion) {

        anim.SetInteger("state", 1);    // transita a "andando"

        switch (direccion){
            case 0:                     // izquierda
                rb.AddForce(new Vector2(-VELOCIDAD, 0),ForceMode2D.Force);
                break;
            case 1:                     // derecha
                rb.AddForce(new Vector2(VELOCIDAD, 0), ForceMode2D.Force);
                break;
       /*     case 2:     // salto / subir
                Vector3 nuevaP3 = new Vector3(Jugador.transform.position.x, Jugador.transform.position.y, Jugador.transform.position.z);
                Jugador.transform.position = nuevaP3;
                break;
            case 3:     // bajar
                Vector3 nuevaP4 = new Vector3(Jugador.transform.position.x, Jugador.transform.position.y, Jugador.transform.position.z);
                Jugador.transform.position = nuevaP4;
                break;
        */
            default:
                break;
        }
        
    }

    /** 
     * salto
     * Gestiona el salto aplicando fuerzas
    */
    void salto()
    {
        anim.SetInteger("state", 2);    // transita a "saltando"
        if (puede_saltar == true)       // verifica que todo esta listo para dar un buen salto
        {
            rb.AddForce(new Vector2(0, ALTURA), ForceMode2D.Impulse);       // agrega fuerza hacia arriba
        }
    }


    /*****************************************************
                INTERFACES
    *****************************************************/
    /**
     * Damage
     * interfaz para recibir daño
     */
    public void Damage(float damageTaken)
    {
        vidaCurr = vidaCurr - damageTaken;
        if ((damageTaken < 0)&&(vidaCurr > vidaMax))                 // restaura salud: si modificamos esta funcion un poco, podemos ahorrarnos una interfaz para ganar salud
        {
            vidaCurr = vidaMax;                                      // no restaura mas salud de la permitida
        }

        //img.color = Color.Lerp(rojo, verde, vidaCurr / vidaMax);     // divide la barra de vida en la porcion correspondiente y la pinta
        barraVida.value = vidaCurr / 100;                           // asigna el valor a la barra de vida

      //  Debug.Log("jugador: vida actual = "+vidaCurr);
        // comprueba si sigue vivo
        if (vidaCurr <= 0)
        {
         //   Debug.Log("jugador: muere");
            // hay que matarlo
            Kill();
        }
    }

    /**
     * Kill
     * interfaz para morir
     */
    public void Kill()
    {
        SceneManager.LoadScene("gameOver");
        Destroy(this.gameObject);
    
    }


    /*************************************
            LISTENERS
    **************************************/

    // Se ejecuta siempre que el objeto entra en contacto con un collider por primera vez.
    void OnCollisionEnter2D(Collision2D col)
    {    
        if(col.gameObject.tag == "Plataforma")      // comprueba que el otro objeto tiene la etiqueta Plataforma
        {
            puede_saltar = true;                    // el personaje puede saltar
        }
    }

    // Se ejecuta siempre que el objeto permanezca en contacto con un collider. Puede ejecutarse muchas veces.
    void OnCollisionStay2D(Collision2D col) {
        if(col.gameObject.tag == "Plataforma")
        {
            puede_saltar = true;
        }
    }

    // Se ejecuta siempre que el objeto deje de estar en contacto con un collider.
    void OnCollisionExit2D(Collision2D col)
    {
        if(col.gameObject.tag == "Plataforma")
        {
            puede_saltar = false;
        }
        
    }
    
}
