﻿using UnityEngine;
using System.Collections;

public class accionesProta : MonoBehaviour {
    public float vNatural;      // max vel del protagonista en condiciones normales
    public float vUmbral;       // divisor entre estados natural e impulsado
    public float vSalto;        // max vel del protagonista durante el salto en el eje y
    public float jumpControl;   // velocidad del prota cuando esta en el aire
    public float pulseControl;  // control del prota cuando esta siendo impulsado
    public float fuerzaSalto = 10000f;   // fuerza del salto natural

    ////////////////////////
    public float ejeHoriz;
 
    ////////////////////////
    public float mediAltura = 0.1f;    // distancia entre el suelo y el pivote

    public float siguiente_ataque;
    public float t_ataque;
    public float fuerza_retroceso = 1000f;
    public GameObject balas;

    private KeyCode ir_derecha = KeyCode.D;
    private KeyCode ir_izquierda = KeyCode.A;
    private KeyCode saltar = KeyCode.W;
    private KeyCode shootUp = KeyCode.UpArrow;
    private KeyCode shootDown = KeyCode.DownArrow;
    private KeyCode shootLeft = KeyCode.LeftArrow;
    private KeyCode shootRight = KeyCode.RightArrow;

    private bool input_der;
    private bool input_izq;

    private bool shooting = false;

    private Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        shooting = false;
        ejeHoriz = Input.GetAxis("Horizontal");
        
        input_der = Input.GetKey(ir_derecha);
        input_izq = Input.GetKey(ir_izquierda);



        /** SALTO Y DISPARO **/
        if (Input.GetKeyDown(saltar) && IsGrounded)
        {
            rb.AddForce(transform.up * fuerzaSalto, ForceMode2D.Impulse);
        }
        if (Input.GetKey(shootRight))
        {
            Disparar(transform.right, 1);
        }
        else
        {
            if (Input.GetKey(shootLeft))
            {
                Disparar(transform.right, -1);
            }
            else
            {
                if (Input.GetKey(shootUp))
                {
                    Disparar(transform.up, 1);
                }
                else
                {
                    if (Input.GetKey(shootDown))
                    {
                        Disparar(transform.up, -1);
                    }
                }
            }
        }

    }


    // Aplicacion de fuerzas
    void FixedUpdate()
    {
        /** MOVIMIENTO **/
        // mira la velocidad
        if(rb.velocity.magnitude > vUmbral)
        {
            // en el aire y no mucha velocidad : aereo
            if (!IsGrounded && Mathf.Abs(rb.velocity.y) < vSalto)
            {
                moverse(1);
            }
            // alta velocidad o en tierra : impulsado
            else
            {
                moverse(2);
            }
        }
        // poca velocidad
        else
        {
            // si esta en tierra firme : natural
            if (IsGrounded)
            {
                moverse(0);
            }
            // si no esta en tierra firme : aereo
            else
            {
                moverse(1);
            }
        }
        /**CORRECCIONES**/

    }

    /*public enum TipoMovimiento
    {
        Natural,
        Aereo,
        Impulsado
    }*/
    /***/
    public void moverse( int tipo_mov)
    {
        switch (tipo_mov)
        {
            // natural
            // case (int) TipoMovimiento.Natural:
            case 0:
              if (ejeHoriz != 0)
                {
                    rb.velocity = new Vector2(vNatural * ejeHoriz, rb.velocity.y);
                }
                else
                {
                    if (!shooting) {
                        rb.velocity = new Vector2(vNatural * 0.000001f, rb.velocity.y);
                    }
                }
                break;
            // aereo
            case 1:
                if (ejeHoriz != 0)
                {
                    rb.AddForce(transform.right * jumpControl * ejeHoriz, ForceMode2D.Force);
                }    
                break;
            // impulsado
            case 2:
                if (input_der)
                {
                    rb.AddForce(transform.right * pulseControl/rb.velocity.magnitude, ForceMode2D.Force);
                }
                else
                {
                    if(input_izq)
                        rb.AddForce(-transform.right * pulseControl/ rb.velocity.magnitude, ForceMode2D.Force);
                }
                break;
            // caso no contemplado
            default:
                Debug.Log("fallo en funcion de movimiento");
                break;
        }
    }

    /****
    public bool isGrounded()
    {
        RaycastHit2D[] hit = new RaycastHit2D[2];
        if (rb.Cast(-transform.up, hit, mediAltura) > 0)
        {
            if (hit[0].collider != null)
            {
                return true;
            }
            return false;
        }
        return false;

    }

    */

        

    public bool IsGrounded { get {
        RaycastHit2D[] hit = new RaycastHit2D[1];
        if (rb.Cast(-transform.up, hit, mediAltura) > 0)
        {
            if (hit[0].collider != null && hit[0].collider.tag == "Environment")
            {
                return true;
            }
            return false;
        }
        return false;

        } }
   

    /**
     * Efectua un disparo
     * @arg direccion : linea que define la direccion de la trayectoria. Debe ser vector unario.
     * @arg sentido : determina el sentido del prota. 1 = derecha, -1 = izquierda --- PUEDE SER REDUNDANTE
     */
    public void Disparar(Vector2 direccion, int sent)
    {
        shooting = true;
        if (Time.time > siguiente_ataque)
        {
            GameObject pro = (GameObject)Instantiate(balas, transform.position, Quaternion.identity);
            pro.GetComponent<bala>().direccion = direccion;
            pro.GetComponent<bala>().sentido = sent;
            aplicarRetroceso(direccion, -sent);

            siguiente_ataque = Time.time + t_ataque;
        }
    }


    /**
     * Aplica un ligero impulso en la misma direccion de disparo pero sentido contrario
     * @arg direccion: linea que define la direccion de la trayectoria. Debe ser vector unario.
     */
    public void aplicarRetroceso(Vector2 direccion, int sent)
    {
        if (IsGrounded)
        {
            rb.AddForce(direccion * sent * fuerza_retroceso, ForceMode2D.Impulse);
        }
        rb.AddForce(direccion * sent * fuerza_retroceso, ForceMode2D.Impulse);
    }
}
