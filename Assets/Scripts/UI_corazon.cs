﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UI_corazon : MonoBehaviour {

    public Transform origen;
    public GameObject corazon;
    public Sprite full_name;
    public Sprite half_name;
    public Sprite empty_name;
    public SpriteRenderer sr;
    public int max_container_value = 2;       // vida por contenedor, puede ser 0, 1 o 2
    public int max_containers = 10;
    public int n_containers = 3;
    public int current_container = 2;       // index at 0
    public int value = 2;                   // valor current container
    public int derecha = 2;                 // desplazamiento a la derecha

    private List<GameObject> corazones;

    // Use this for initialization
    void Start() {
        // crea y pone los corazones
        corazones = new List<GameObject>();
        origen = GetComponent<Transform>();
        float xComp = origen.position.x;
        Vector3 vec;
        for ( int i = 0; i < n_containers; i++)
        {
            xComp = origen.position.x + (i * derecha);
            vec = new Vector3(xComp, origen.position.y, origen.position.z);
            corazones.Add((GameObject)Instantiate(corazon, vec, Quaternion.identity));
            corazones[i].GetComponent<Transform>().parent = origen;
        }

	}

    //-11.79
    //6.42

    public void actualizarSprite(int indice)
    {
        sr = corazones[indice].GetComponent<SpriteRenderer>();
        switch (value)
        {
            case 0:
                sr.sprite = empty_name;
                break;
            case 1:
                sr.sprite = half_name;
                break;
            case 2:
                sr.sprite = full_name;
                break;
            default:
                sr.sprite = full_name;
                value = 2;
                break;
        }
    }

    // -1 hp
    public void pum()
    {
        switch (value)
        {
            case 0:
                // retrocede 1 corazon si puede
                if(current_container != 0)
                {
                    current_container--;
                    value = 1;
                }


                break;
            case 1:
                value = 0;
                break;
            case 2:
                value = 1;
                break;
            default:
                // ... 
                break;
        }
        actualizarSprite(current_container);
    }


    // +1 hp
    public void miniheal()
    {
        switch (value)
        {
            case 0:
                value = 1;

                break;
            case 1:
                value = 2;
                break;
            case 2:
                // avanza corazon si puede
                if(current_container < n_containers-1)
                {
                    current_container++;
                    value = 1;
                }
                break;
            default:
                // ... 
                break;
        }
        actualizarSprite(current_container);
    }

    // Update is called once per frame
    public void restaurarSalud(int cantidad)
    {
        for (int i = 0; i < cantidad; i++) {
            miniheal();
        }
    }
    // Update is called once per frame
    public void quitarSalud(int cantidad)
    {
        for (int i = 0; i < cantidad; i++)
        {
            pum();
        }
    }

    public void incrementarNContainers(int nKontainers)
    {
      
    }

    public void decrementarNContainers(int nKontainers)
    {

       
    }

    public void agregarContainer()
    {
        Vector3 vec;
        vec = new Vector3(origen.position.x+(n_containers*derecha), origen.position.y, origen.position.z);
        corazones.Add((GameObject)Instantiate(corazon, vec, Quaternion.identity));
        corazones[n_containers].GetComponent<Transform>().parent = origen;
        n_containers++;
        
    }

    public void quitarContainer()
    {
        int i = n_containers - 1;
        Destroy(corazones[i].gameObject);
        n_containers--;
        current_container = n_containers - 1;
        
    }
}
